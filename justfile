build:
    mkdir -p dist
    cp vendor/* dist/
    bunx tailwindcss -i index.css -o dist/index.css
    cp index.html dist/

watch:
    bunx parcel

deploy:
    mkdir -p dist_prod
    bunx parcel build --public-url ./ --dist-dir dist_prod
    rsync -vrhaP --delete $(pwd)/dist_prod/ agdsn.me:public_html/colorneutral
