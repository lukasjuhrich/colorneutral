# Color Picker
A tool to help you become [color neutral](https://speedsolving.fandom.com/wiki/Color_Neutrality) in speedcubing.

Hosted at [https://agdsn.me/~lukasjuhrich/colorneutral/](https://agdsn.me/~lukasjuhrich/colorneutral/).

Made with [tailwind](https://tailwindcss.com/) and [hyperscript](https://hyperscript.org/).

---

# Dev setup

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run index.html
```

This project was created using `bun init` in bun v1.1.3. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.
